# mini-expect

![](https://img.shields.io/badge/written%20in-bash-blue)

An expect(1) clone that doesn't corrupt CR bytes.

I recently attempted to use expect(1) as part of a test suite, but it seems totally incapable of sending 0x13 bytes without corrupting them into 0x10. Maybe it was my terminal mode (but `echo $'\r' | nc` was fine), maybe it was my expect/tcl installation (but the bug was observed on current packages from both Cygwin and Debian), maybe it was easier to reimplement the basic functionality instead of spending any more time on that.

## Usage

`source mini-expect.sh`

Then, use spawn/send/expect/interact commands just like with expect(1).


## Download

- [⬇️ mini-expect.v1.sh](dist-archive/mini-expect.v1.sh) *(474B)*
