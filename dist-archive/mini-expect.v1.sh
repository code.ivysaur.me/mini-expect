#!/bin/bash
# 
# Mini expect(1) clone that doesn't mangle \r into \n.
#

EXPECT_IN=0
EXPECT_OUT=1

spawn() {
	coproc "$@"
	EXPECT_IN=${COPROC[0]}
	EXPECT_OUT=${COPROC[1]}
}

expect() {
	local expect_str="$1"
	while read -r line <&${EXPECT_IN} ; do
		echo "$line"
		if [[ $line =~ $expect_str ]] ; then
			return
		fi
	done
}

send() {
	echo -n "$@"
	echo -n "$@" >&${EXPECT_OUT}
}

send_pipe() {
	local buffer="$(cat)"
	send "$buffer"
}

interact() {
	cat >&${EXPECT_OUT}
}
